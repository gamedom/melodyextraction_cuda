
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <thread>

#include <stdio.h>

#define M_PI 3.14159265358979323846f

#define NUMBER_OF_HARMONICS 20
#define HARMONIC_WEIGHTING_PARAM 0.9f
#define MAGNITUDE_COMPRESSION_RATE 1.0f
#define MAXIMUM_ALLOWED_dB 40
#define SALIENCE_SIZE 600

//For debugging
float debug_code_progress_coeff = 0;

float* freqs;
float* amps;
float* maxAmps; // maximum amplitude for every frame.
float* Salience;
int numOfFrames;
int numOfSamples;

float* Spectrum;
float* Peaks;
int* PeaksFoundIndexes;
int peaksIndexesFrameSize;

//threading stuff
std::thread SalienceThread;
bool salienceThreadFinished = false;
bool salienceThreadStarted = false;

//definition of functions

//Memory related functions
void DeallocateArray(float** arr, int dimSize);
void DeallocateArray(int** arr, int dimSize);
void DeallocateArray(float* arr);
void DeallocateArray(int* arr);

//ME based functions
void ComputeSalience();
void ComputeParabolicInterpolation();
void ComputeMaxAmps();

__device__ float GetNormalisedMS(float x);
__device__ float ComputeHarmonicWeighting(float bin, int harmonicNum, float freq);
__device__ float ComputeFreqFromHzToSalienceBin(float freq);
__device__ int ComputeResidualAmp(float ampp, float maxAmp);


//CUDA kernels
__global__ void ME_CudaSalienceFrame(
	float *debug_coeff,
	float *results,
	const float *amps,
	const float *freqs,
	const float *spectrum,
	const int *peaksFoundIndexes,
	const float *maxAmps,
	const int *peaksFoundIndexesSize,
	const int *numOfSamples,
	float *harmonicSums);
//__global__ void ME_CudaSalienceBins();
//__global__ void ME_CudaSaliencePeaks();
__global__ void ME_CudaComputeParabolicInterpolation(float *freqs, float *amps, const float *Peaks, const int *numOfSamples);
__global__ void ME_CudaComputeMaxAmplitudes(float *maxAmps, const float *amps, const int *peaksFoundIndexes, const int *peaksFrameSize, const int *numOfSamples);

// Every function that communicates with C# code goes here.
extern "C" 
{
	// Init internal memory
	__declspec(dllexport) void InitializeME(int frames, int samples, int peakIndexesSize)
	{
		numOfFrames = frames;
		numOfSamples = samples;
		peaksIndexesFrameSize = peakIndexesSize;

		Spectrum = new float[numOfFrames * numOfSamples];
		Peaks = new float[numOfFrames * numOfSamples];
		PeaksFoundIndexes = new int[numOfFrames * peaksIndexesFrameSize];

		Salience = new float[numOfFrames * SALIENCE_SIZE];
		freqs = new float[numOfFrames * numOfSamples];
		amps = new float[numOfFrames * numOfSamples];

		maxAmps = new float[numOfFrames];

		/*for (int i = 0; i < numOfFrames; i++)
		{
			Spectrum[i] = new float[numOfSamples];
			Peaks[i] = new float[numOfSamples];
			PeaksFoundIndexes[i] = new int[peaksIndexesFrameSize];

			Salience[i] = new float[SALIENCE_SIZE];
			freqs[i] = new float[numOfSamples];
			amps[i] = new float[numOfSamples];
		}*/

		salienceThreadFinished = false;
		salienceThreadStarted = false;
	}

	// Copy received input data to internal memory
	// Returns a status code:
	// -1 : success
	// 1 : failure
	__declspec(dllexport) int SendInputData(
		float* spectrumFrame, 
		float* peaksFrame, 
		int* peaksIndexesFrame, 
		int frameIndex)
	{
		if (spectrumFrame == nullptr ||
			peaksFrame == nullptr ||
			peaksIndexesFrame == nullptr ||
			(frameIndex < 0 && frameIndex >= numOfFrames))
		{
			return 1;
		}

		for (int i = 0; i < numOfSamples; i++)
		{
			Spectrum[frameIndex * numOfSamples + i] = spectrumFrame[i];
			Peaks[frameIndex * numOfSamples + i] = peaksFrame[i];
		}

		for (int i = 0; i < peaksIndexesFrameSize; i++)
		{
			PeaksFoundIndexes[frameIndex * peaksIndexesFrameSize + i] = peaksIndexesFrame[i];
		}

		return -1;
	}


	__declspec(dllexport) int GetInputData(float *spectrumFrame, float *peaksFrame, int *peaksIndexesFrame, int frameIndex)
	{
		for (int i = 0; i < numOfSamples; i++)
		{
			spectrumFrame[i] = Spectrum[frameIndex * numOfSamples + i];
			peaksFrame[i] = Peaks[frameIndex * numOfSamples + i];
		}

		for (int i = 0; i < peaksIndexesFrameSize; i++)
		{
			peaksIndexesFrame[i] = PeaksFoundIndexes[frameIndex * peaksIndexesFrameSize + i];
		}

		return frameIndex;
	}

	//For debugging
	__declspec(dllexport) float GetDebugCodeProgress()
	{
		return debug_code_progress_coeff;
	}

	// Starts up the salience thread, that will compute the salience
	__declspec(dllexport) void StartSalienceThread()
	{
		if (salienceThreadStarted)
			return;

		if (SalienceThread.joinable())
			SalienceThread.join();
		SalienceThread = std::thread(ComputeSalience);
		salienceThreadStarted = true;
		salienceThreadFinished = false;
	}

	__declspec(dllexport) void SetIdleSalienceThread()
	{
		salienceThreadFinished = false;
		salienceThreadStarted = false;
	}

	// Returns a status code of SalienceThread
	// codes:
	// 1 - thread started
	// 2 - thread finished
	// 3 - thread not started, nor finished
	__declspec(dllexport) int GetSalienceComputingStatus()
	{
		return salienceThreadStarted ? 1 : (salienceThreadFinished ? 2 : 3);
	}

	// Gets to Managed code Salience frame samples
	__declspec(dllexport) void GetSalienceFrame(float* salienceFrame, int frameIndex)
	{
		if (frameIndex < 0 && frameIndex >= numOfFrames)
			return;

		for (int i = 0; i < SALIENCE_SIZE; i++)
		{
			salienceFrame[i] = Salience[frameIndex * SALIENCE_SIZE + i];
		}
	}

	__declspec(dllexport) void DeinitializeME()
	{
		if (SalienceThread.joinable())
			SalienceThread.join();

		DeallocateArray(freqs/*, numOfFrames*/);
		DeallocateArray(amps/*, numOfFrames*/);
		DeallocateArray(Salience/*, numOfFrames*/);

		DeallocateArray(Spectrum/*, numOfFrames*/);
		DeallocateArray(Peaks/*, numOfFrames*/);
		DeallocateArray(PeaksFoundIndexes/*, numOfFrames*/);

		DeallocateArray(maxAmps);
	}

	__declspec(dllexport) int GetNFramesNSamples(bool frames)
	{
		return frames ? numOfFrames : numOfSamples;
	}
}

void DeallocateArray(float** arr, int dimSize)
{
	if (arr == nullptr)
		return;

	for (int i = 0; i < dimSize; i++)
	{
		delete[] arr[i];
		arr[i] = nullptr;
	}

	delete[] arr;
	arr = nullptr;
}

void DeallocateArray(int** arr, int dimSize)
{
	if (arr == nullptr)
		return;

	for (int i = 0; i < dimSize; i++)
	{
		delete[] arr[i];
		arr[i] = nullptr;
	}

	delete[] arr;
	arr = nullptr;
}

void DeallocateArray(float* arr)
{
	if (arr == nullptr)
		return;

	delete[] arr;
	arr = nullptr;
}

void DeallocateArray(int* arr)
{
	if (arr == nullptr)
		return;

	delete[] arr;
	arr = nullptr;
}

// Computing of salience starts here
void ComputeSalience()
{
	ComputeParabolicInterpolation();

	ComputeMaxAmps();

	float* dev_spectrum;
	float* dev_amps;
	float* dev_freqs;
	int* dev_peaksFoundIndexes;
	float* dev_maxAmps;
	int* dev_peaksFoundIndexesSize;
	int *dev_numOfSamples;
	float* dev_debug_coeff;

	float *dev_harmonicSums;

	float* dev_results;
	//float* results = new float[SALIENCE_SIZE];

	cudaMalloc((void**)&dev_spectrum, numOfFrames * numOfSamples * sizeof(float));
	cudaMalloc((void**)&dev_amps, numOfFrames * numOfSamples * sizeof(float));
	cudaMalloc((void**)&dev_freqs, numOfFrames * numOfSamples * sizeof(float));
	cudaMalloc((void**)&dev_peaksFoundIndexes, numOfFrames * peaksIndexesFrameSize * sizeof(int));
	cudaMalloc((void**)&dev_results, numOfFrames * SALIENCE_SIZE * sizeof(float));
	cudaMalloc((void**)&dev_maxAmps, numOfFrames * sizeof(float));
	cudaMalloc((void**)&dev_peaksFoundIndexesSize, sizeof(int));
	cudaMalloc((void**)&dev_numOfSamples, sizeof(int));
	cudaMalloc((void**)&dev_debug_coeff, sizeof(float));

	cudaMalloc((void**)&dev_harmonicSums, numOfFrames * SALIENCE_SIZE * sizeof(float));

	//for (int i = 0; i < numOfFrames; i++)
	//{

		cudaMemcpy(dev_spectrum, Spectrum, numOfFrames * numOfSamples * sizeof(float), cudaMemcpyHostToDevice);
		cudaMemcpy(dev_amps, amps, numOfFrames * numOfSamples * sizeof(float), cudaMemcpyHostToDevice);
		cudaMemcpy(dev_freqs, freqs, numOfFrames * numOfSamples * sizeof(float), cudaMemcpyHostToDevice);
		cudaMemcpy(dev_peaksFoundIndexes, PeaksFoundIndexes, numOfFrames * peaksIndexesFrameSize * sizeof(int), cudaMemcpyHostToDevice);
		cudaMemcpy(dev_maxAmps, maxAmps, numOfFrames * sizeof(float), cudaMemcpyHostToDevice);
		cudaMemcpy(dev_peaksFoundIndexesSize, &peaksIndexesFrameSize, sizeof(int), cudaMemcpyHostToDevice);
		cudaMemcpy(dev_numOfSamples, &numOfSamples, sizeof(int), cudaMemcpyHostToDevice);

		ME_CudaSalienceFrame<<< SALIENCE_SIZE, dim3(numOfFrames, NUMBER_OF_HARMONICS, 1)>>> (
			dev_debug_coeff, 
			dev_results, dev_amps, 
			dev_freqs, dev_spectrum, 
			dev_peaksFoundIndexes, 
			dev_maxAmps, 
			dev_peaksFoundIndexesSize, 
			dev_numOfSamples,
			dev_harmonicSums);

		// Check for any errors launching the kernel
		cudaError_t cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) {
			debug_code_progress_coeff = 2;
			goto ComputeSalience_exit;
		}

		cudaDeviceSynchronize();

		cudaMemcpy(Salience, dev_results, numOfFrames * SALIENCE_SIZE * sizeof(float), cudaMemcpyDeviceToHost);
		int debug_coeff = 0;
		cudaMemcpy(&debug_coeff, dev_debug_coeff, sizeof(float), cudaMemcpyDeviceToHost);
		debug_code_progress_coeff += debug_coeff;
		//debug_code_progress_coeff++;
		////memcpy(Salience[i], results, SALIENCE_SIZE * sizeof(float));

		/*for (int j = 0; j < SALIENCE_SIZE; j++)
		{
			Salience[i * SALIENCE_SIZE + j] = results[j];
		}*/
		//debug_code_progress_coeff++;
	//}

	//DeallocateArray(results);
ComputeSalience_exit:
	cudaFree(dev_spectrum);
	cudaFree(dev_amps);
	cudaFree(dev_freqs);
	cudaFree(dev_peaksFoundIndexes);
	cudaFree(dev_results);
	cudaFree(dev_maxAmps);
	cudaFree(dev_peaksFoundIndexesSize);
	cudaFree(dev_harmonicSums);

	salienceThreadFinished = true;
	salienceThreadStarted = false;
}

void ComputeParabolicInterpolation()
{
	//float diff = 0.0f, a1, a2, a3;

	float *dev_peaks;
	float *dev_freqs;
	float *dev_amps;
	int *dev_nSamples;
	
	cudaMalloc((void**)&dev_peaks, numOfFrames * numOfSamples * sizeof(float));
	cudaMalloc((void**)&dev_freqs, numOfFrames * numOfSamples * sizeof(float));
	cudaMalloc((void**)&dev_amps, numOfFrames * numOfSamples * sizeof(float));
	cudaMalloc((void**)&dev_nSamples, sizeof(int));

	//for (int i = 0; i < numOfFrames; i++)
	//{
		cudaMemcpy(dev_peaks, Peaks, numOfFrames * numOfSamples * sizeof(float), cudaMemcpyHostToDevice);
		cudaMemcpy(dev_nSamples, &numOfSamples, sizeof(int), cudaMemcpyHostToDevice);

		ME_CudaComputeParabolicInterpolation <<< dim3(numOfSamples / 256, numOfFrames, 1), 256 >>>(dev_freqs, dev_amps, dev_peaks, dev_nSamples);

		// Check for any errors launching the kernel
		cudaError_t cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) {
			debug_code_progress_coeff = 3;
			goto ComputeParabolicInterpolation_exit;
		}

		cudaDeviceSynchronize();

		cudaMemcpy(freqs, dev_freqs, numOfFrames * numOfSamples * sizeof(float), cudaMemcpyDeviceToHost);
		cudaMemcpy(amps, dev_amps, numOfFrames * numOfSamples * sizeof(float), cudaMemcpyDeviceToHost);

	//}
ComputeParabolicInterpolation_exit:
	cudaFree(dev_peaks);
	cudaFree(dev_amps);
	cudaFree(dev_freqs);
	cudaFree(dev_nSamples);
}

void ComputeMaxAmps()
{
	float *dev_maxAmps, *dev_amps;
	int *dev_peaksFrameSize, *dev_numOfSamples, *dev_peaksFoundIndexes;

	cudaMalloc((void**)&dev_amps, numOfSamples * numOfFrames * sizeof(float));
	cudaMalloc((void**)&dev_peaksFoundIndexes, peaksIndexesFrameSize * numOfFrames * sizeof(int));
	cudaMalloc((void**)&dev_maxAmps, numOfFrames * sizeof(float));
	cudaMalloc((void**)&dev_peaksFrameSize, sizeof(int));
	cudaMalloc((void**)&dev_numOfSamples, sizeof(int));

	cudaMemcpy(dev_amps, amps, numOfSamples * numOfFrames * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(dev_peaksFoundIndexes, PeaksFoundIndexes, peaksIndexesFrameSize * numOfFrames * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(dev_peaksFrameSize, &peaksIndexesFrameSize, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(dev_numOfSamples, &numOfSamples, sizeof(int), cudaMemcpyHostToDevice);

	ME_CudaComputeMaxAmplitudes<<< 1, numOfFrames >>>(dev_maxAmps, dev_amps, dev_peaksFoundIndexes, dev_peaksFrameSize, dev_numOfSamples);

	// Check for any errors launching the kernel
	cudaError_t cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		goto ComputeMaxAmps_exit;
		debug_code_progress_coeff = 1;
	}

	cudaDeviceSynchronize();

	cudaMemcpy(maxAmps, dev_maxAmps, numOfFrames * sizeof(float), cudaMemcpyDeviceToHost);
	
ComputeMaxAmps_exit:
	cudaFree(dev_amps);
	cudaFree(dev_peaksFoundIndexes);
	cudaFree(dev_maxAmps);
	cudaFree(dev_peaksFrameSize);
	cudaFree(dev_numOfSamples);
}

__device__ float GetNormalisedMS(float x)
{
	return x > 0.0f ? 20 * log10f(x) : 0.0f;
}

__device__ float ComputeHarmonicWeighting(float bin, int harmonicNum, float freq)
{
	float distanceInSemitones = fabs(ComputeFreqFromHzToSalienceBin(freq / harmonicNum) - bin) / 10;
	if (distanceInSemitones <= 1)
	{
		return powf(cosf(distanceInSemitones * M_PI / 2), 2) * powf(HARMONIC_WEIGHTING_PARAM, harmonicNum - 1);
	}
	else
	{
		return 0.0f;
	}
}

__device__ float ComputeFreqFromHzToSalienceBin(float freq)
{
	float temp = freq / 55;
	if (temp > 0)
	{
		return floorf((1200 * log2f(temp)) / 10 + 0.5f);
	}
	else
	{
		return 0.0f;
	}
}

__device__ int ComputeResidualAmp(float ampp, float maxAmp)
{
	return maxAmp / ampp < MAXIMUM_ALLOWED_dB ? 1 : 0;
}


__global__ void ME_CudaSalienceFrame(
	float *debug_coeff,
	float *results,
	const float *amps,
	const float *freqs,
	const float *spectrum,
	const int *peaksFoundIndexes,
	const float *maxAmps,
	const int *peaksFoundIndexesSize,
	const int *numOfSamples,
	float *harmonicSums)
{
	int frameIndex = threadIdx.x;
	int binIndex = blockIdx.x;
	int harmonicNumber = threadIdx.y + 1;
	int index = threadIdx.x + blockIdx.x * blockDim.x;

	float peakSum = 0.0f;
	for (int peakIndex = 0; peakIndex < *peaksFoundIndexesSize; peakIndex++)
	{
		float residualAmp = ComputeResidualAmp(amps[(frameIndex) * (*numOfSamples) + peaksFoundIndexes[(frameIndex) * (*peaksFoundIndexesSize) + peakIndex]], maxAmps[(frameIndex)]);
		float harmonicWeighting = ComputeHarmonicWeighting(spectrum[(frameIndex) * (*numOfSamples) + (binIndex)], harmonicNumber, freqs[(frameIndex) * (*numOfSamples) + peaksFoundIndexes[(frameIndex) * (*peaksFoundIndexesSize) + peakIndex]]);
		float powAmp = powf(amps[(frameIndex) * (*numOfSamples) + peaksFoundIndexes[(frameIndex) * (*peaksFoundIndexesSize) + peakIndex]], MAGNITUDE_COMPRESSION_RATE);
		peakSum += residualAmp * harmonicWeighting * powAmp;
	}


	__syncthreads();

	atomicAdd(&harmonicSums[index], frameIndex + binIndex * blockDim.x == index ? peakSum : 0.0f);

	results[(frameIndex) * SALIENCE_SIZE + (binIndex)] = harmonicSums[index];
}

__global__ void ME_CudaComputeParabolicInterpolation(float* freqs, float* amps, const float* Peaks, const int* numOfSamples)
{
	int j = threadIdx.x + blockIdx.x * blockDim.x;
	int frameIndex = blockIdx.y;

	float diff = 0.0f, a1, a2, a3;

	a1 = GetNormalisedMS(j == 0 ? 0 : Peaks[frameIndex * (*numOfSamples) + (j - 1)]);
	a2 = GetNormalisedMS(Peaks[frameIndex * (*numOfSamples) + j]);
	a3 = GetNormalisedMS(j == (frameIndex * (*numOfSamples) + (*numOfSamples - 1)) ? 0 : Peaks[frameIndex * (*numOfSamples) + (j + 1)]);
	float nominator = (a1 - 2 * a2 + a3);

	if (nominator != 0)
		diff = 0.5f * (a1 - a3) / nominator;
	else
		diff = 0.0f;

	freqs[frameIndex * (*numOfSamples) + j] = (Peaks[frameIndex * (*numOfSamples) + j] + diff <= 80 && Peaks[frameIndex * (*numOfSamples) + j] + diff > -80 ? Peaks[frameIndex * (*numOfSamples) + j] + diff : 0) * 44100 / (*numOfSamples * 2);
	amps[frameIndex * (*numOfSamples) + j] = a2 - (diff / 4) * (a1 - a3);
}

__global__ void ME_CudaComputeMaxAmplitudes(float *maxAmps, const float *amps, const int *peaksFoundIndexes, const int *peaksFrameSize, const int *numOfSamples)
{
	int i = threadIdx.x;

	float maxAmp = -1000.0f;
	for (int j = 0; j < *peaksFrameSize; j++)
	{
		maxAmp = amps[i * (*numOfSamples) + peaksFoundIndexes[i * (*peaksFrameSize) + j]] < maxAmp ? maxAmp : amps[i * (*numOfSamples) + peaksFoundIndexes[i * (*peaksFrameSize) + j]];
	}

	maxAmps[i] = maxAmp;
}